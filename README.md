# Desafio Hotel - API para gerenciar o funcionamento de um hotel

<center><img src="./images/senior-sistemas-logo.png"></center>
---

## Objetivo
O projeto tem como objetivo auxiliar na modernização de um sistema de Hotel.

## O que a API representa:
Base A - **hotel API**
* CRUD de hospedes
* Buscar todos hóspedes por filtros(nome, documento, telefone)
* Check-in
* Check-out
* Buscar todas reservas por filtros(documentoHospede, nomeHospede, telefoneHospede) ou (se os hóspedes ainda estão hospedados no hotel ou não)
* Cálculo de valorTotal/valorDaHospedagem levando em consideração dias de fim de semana, e se o hóspede deseja garantir uma vaga na garagem do hotel.

# Quais as metodologias, arquitetura e design patterns utilizados:
* Swagger API - permite testar e documentar a API.
* Flyway - permite gerar os scripts para criação/inserção das tabelas no banco de dados.
* Criação da classe ApiExcetionHandler - permite "capturar" e tratar todas as exceções do sistema de maneira centralizada 
* Utilização de classes filter para busca de informações - facilita e padroniza as buscas facilitando em muito a vida do front
* Dockerizar a aplicação - facilidade na montagem do ambiente, homogeneidade entre embiente de dev e prod;
* RestControllerPath - centraliza todos os path para requisições 

# Docker
    Foi necessário a criação de um docker-compose para subir o container configurando o banco de dados PostgresSQL.
    Para subir o container:

	docker-compose -f docker-compose.yaml up -d --build 

# Os paths após as APIs subirem 
	Desafio-Hotel API --> http://localhost:8080/swagger-ui.html
	

Yuri Marques da Silva, 19/07/2022