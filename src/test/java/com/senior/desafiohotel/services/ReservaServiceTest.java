package com.senior.desafiohotel.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.senior.desafiohotel.enums.SIM_NAO;
import com.senior.desafiohotel.models.Hospede;
import com.senior.desafiohotel.models.Parametro;
import com.senior.desafiohotel.models.Reserva;
import com.senior.desafiohotel.repositories.ParametroRepository;
import com.senior.desafiohotel.repositories.ReservaRepository;

@ExtendWith(SpringExtension.class)
public class ReservaServiceTest {

    @MockBean
    private ReservaRepository reservaRepository;

    @MockBean
    private ParametroRepository parametroRepository;

    @Test
    @DisplayName("Testando o checkin do hotel")
    void testFazerCheckIn() {
        reservaRepository.save(Reserva.builder()
                .dataEntrada(LocalDateTime.now())
                .hospede(Hospede.builder()
                        .id(1L)
                        .nome("Hospede 1")
                        .build())
                .adicionalVeiculo(SIM_NAO.S)
                .build());

    }

    @Test
    @DisplayName("Testando o checkout do hotel, cálculo de valorTotal e valorHospedagem")
    void testFazerCheckOut() {
        Reserva reserva = Reserva.builder()
                .dataEntrada(LocalDateTime.now().minusDays(3))
                .dataSaida(LocalDateTime.now())
                .adicionalVeiculo(SIM_NAO.S)
                .hospede(Hospede.builder().id(1L).nome("Hospede 1").build())
                .build();

        Parametro parametro = Parametro.builder()
                .valorDiariaDiaDeSemana(120.0)
                .valorDiariaFimDeSemana(150.0)
                .valorGaragemDiaDeSemana(15.0)
                .valorGaragemFimDeSemana(20.0)
                .build();

        Double valorDiariaFimDeSemana = parametro.getValorDiariaFimDeSemana();
        Double valorDiariaDiaDeSemana = parametro.getValorDiariaDiaDeSemana();
        Double valorGaragemFimDeSemana = parametro.getValorGaragemFimDeSemana();
        Double valorGaragemDiaDeSemana = parametro.getValorGaragemDiaDeSemana();

        Double valorTotal = 0.0;
        Double valorUltimaHospedagem = 0.0;

        for (LocalDateTime date = reserva.getDataEntrada(); date
                .isBefore(reserva.getDataSaida()); date = date.plusDays(1)) {
            if (testIsWeekend(date)) {
                if (reserva.getAdicionalVeiculo() == SIM_NAO.S) {
                    valorTotal = valorTotal + valorGaragemFimDeSemana;
                }
                valorTotal = valorTotal + valorDiariaFimDeSemana;
                valorUltimaHospedagem = valorUltimaHospedagem + valorDiariaFimDeSemana;
            } else {
                if (reserva.getAdicionalVeiculo() == SIM_NAO.S) {
                    valorTotal = valorTotal + valorGaragemDiaDeSemana;
                }
                valorTotal = valorTotal + valorDiariaDiaDeSemana;
                valorUltimaHospedagem = valorUltimaHospedagem + valorDiariaDiaDeSemana;
            }
        }

        if (reserva.getDataSaida().isAfter(LocalDateTime.now().withHour(16).withMinute(30).withSecond(0))) {
            if (testIsWeekend(reserva.getDataSaida())) {
                valorTotal = valorTotal + valorDiariaFimDeSemana;
                valorUltimaHospedagem = valorUltimaHospedagem + valorDiariaFimDeSemana;
            } else {
                valorTotal = valorTotal + valorDiariaDiaDeSemana;
                valorUltimaHospedagem = valorUltimaHospedagem + valorDiariaDiaDeSemana;
            }
        }

        // ASSERTING VALOR TOTAL
        assertEquals(595.0, valorTotal);

        // ASSERTING VALOR ULTIMA HOSPEDAGEM
        assertEquals(540.0, valorUltimaHospedagem);

    }

    @Test
    boolean testIsWeekend(LocalDateTime ld) {
        DayOfWeek day = DayOfWeek.of(ld.get(ChronoField.DAY_OF_WEEK));
        return day == DayOfWeek.SUNDAY || day == DayOfWeek.SATURDAY;

    }

}
