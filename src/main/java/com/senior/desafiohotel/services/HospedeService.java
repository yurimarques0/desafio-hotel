package com.senior.desafiohotel.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.senior.desafiohotel.dtos.out.HospedeDTOout;
import com.senior.desafiohotel.filters.HospedeFilter;
import com.senior.desafiohotel.models.Hospede;
import com.senior.desafiohotel.repositories.HospedeRepository;

@Service
public class HospedeService {
    @Autowired
    private HospedeRepository hospedeRepository;

    @Transactional(readOnly = true)
    public List<HospedeDTOout> findAllFiltered(HospedeFilter filter) {
        return hospedeRepository.findAllFiltered(filter);
    }

    @Transactional(readOnly = true)
    public Page<HospedeDTOout> findAllPagedFiltered(Pageable pageable, HospedeFilter filter) {
        return hospedeRepository.findAllPagedFiltered(pageable, filter);
    }

    @Transactional(readOnly = true)
    public Optional<Hospede> findById(Long id) {
        return hospedeRepository.findById(id);
    }

    public Hospede create(Hospede entity) {
        return hospedeRepository.save(entity);
    }

    public Hospede update(Hospede entity) {
        return hospedeRepository.save(entity);
    }

    public void deleteById(Long id) {
        hospedeRepository.deleteById(id);
    }

}
