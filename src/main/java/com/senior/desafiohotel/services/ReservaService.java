package com.senior.desafiohotel.services;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.senior.desafiohotel.dtos.in.ReservaDTOin;
import com.senior.desafiohotel.dtos.out.ReservaDTOout;
import com.senior.desafiohotel.enums.SIM_NAO;
import com.senior.desafiohotel.exceptions.BusinessException;
import com.senior.desafiohotel.exceptions.EntityInUseException;
import com.senior.desafiohotel.exceptions.entities.HospedeNotFoundException;
import com.senior.desafiohotel.exceptions.entities.ParametroNotFoundException;
import com.senior.desafiohotel.exceptions.entities.ReservaNotFoundException;
import com.senior.desafiohotel.filters.ReservaFilter;
import com.senior.desafiohotel.models.Hospede;
import com.senior.desafiohotel.models.Parametro;
import com.senior.desafiohotel.models.Reserva;
import com.senior.desafiohotel.repositories.HospedeRepository;
import com.senior.desafiohotel.repositories.ParametroRepository;
import com.senior.desafiohotel.repositories.ReservaRepository;

@Service
public class ReservaService {

    @Autowired
    private ReservaRepository reservaRepository;

    @Autowired
    private HospedeRepository hospedeRepository;

    @Autowired
    private ParametroRepository parametroRepository;

    public Reserva fazerCheckIn(ReservaDTOin reservaDTOin) {
        Reserva reserva = new Reserva();

        reservaRepository.findByHospedeIdAndDataSaidaIsNull(reservaDTOin.getHospedeId()).ifPresent(c -> {
            throw new EntityInUseException(
                    "A reserva " + c.getId() + " ainda está aberta com o hóspede " + reservaDTOin.getHospedeId());
        });
        ;
        Hospede hospede = hospedeRepository.findById(reservaDTOin.getHospedeId())
                .orElseThrow(() -> new HospedeNotFoundException(reservaDTOin.getHospedeId()));

        if (reservaDTOin.getAdicionalVeiculo() == null) {
            throw new BusinessException("É obrigatório informar se deseja vaga na garagem do hotel");
        }
        reserva.setHospede(hospede);
        reserva.setDataEntrada(LocalDateTime.now());
        reserva.setAdicionalVeiculo(reservaDTOin.getAdicionalVeiculo());

        return reservaRepository.save(reserva);
    }

    public Reserva fazerCheckOut(Long hospedeId) {
        hospedeRepository.findById(hospedeId).orElseThrow(() -> new HospedeNotFoundException(hospedeId));

        Reserva reserva = reservaRepository.findByHospedeIdAndDataSaidaIsNull(hospedeId).orElseThrow(
                () -> new ReservaNotFoundException("Não foi encontrado reservas ativas com o hóspede " + hospedeId));
                
        Parametro parametro = parametroRepository.findById(1L).orElseThrow(() -> new ParametroNotFoundException(1L));

        LocalDateTime now = LocalDateTime.now();
        Double valorTotal = 0.0;
        Double valorUltimaHospedagem = 0.0;
        reserva.setDataSaida(now);

        Double valorDiariaFimDeSemana = parametro.getValorDiariaFimDeSemana();
        Double valorDiariaDiaDeSemana = parametro.getValorDiariaDiaDeSemana();
        Double valorGaragemFimDeSemana = parametro.getValorGaragemFimDeSemana();
        Double valorGaragemDiaDeSemana = parametro.getValorGaragemDiaDeSemana();

        for (LocalDateTime date = reserva.getDataEntrada(); date
                .isBefore(reserva.getDataSaida()); date = date.plusDays(1)) {
            if (isWeekend(date)) {
                if (reserva.getAdicionalVeiculo() == SIM_NAO.S) {
                    valorTotal = valorTotal + valorGaragemFimDeSemana;
                }
                valorTotal = valorTotal + valorDiariaFimDeSemana;
                valorUltimaHospedagem = valorUltimaHospedagem + valorDiariaFimDeSemana;
            } else {
                if (reserva.getAdicionalVeiculo() == SIM_NAO.S) {
                    valorTotal = valorTotal + valorGaragemDiaDeSemana;
                }
                valorTotal = valorTotal + valorDiariaDiaDeSemana;
                valorUltimaHospedagem = valorUltimaHospedagem + valorDiariaDiaDeSemana;
            }
        }

        if (reserva.getDataSaida().isAfter(LocalDateTime.now().withHour(16).withMinute(30).withSecond(0))) {
            if (isWeekend(now)) {
                valorTotal = valorTotal + valorDiariaFimDeSemana;
                valorUltimaHospedagem = valorUltimaHospedagem + valorDiariaFimDeSemana;
            } else {
                valorTotal = valorTotal + valorDiariaDiaDeSemana;
                valorUltimaHospedagem = valorUltimaHospedagem + valorDiariaDiaDeSemana;
            }
        }

        reserva.setValorTotal(valorTotal);
        reserva.setValorUltimaHospedagem(valorUltimaHospedagem);
        reservaRepository.save(reserva);
        return reserva;
    }

    public static boolean isWeekend(final LocalDateTime ld) {
        DayOfWeek day = DayOfWeek.of(ld.get(ChronoField.DAY_OF_WEEK));
        return day == DayOfWeek.SUNDAY || day == DayOfWeek.SATURDAY;
    }

    public List<ReservaDTOout> findAllFiltered(ReservaFilter filter) {
        return reservaRepository.findAllFiltered(filter);
    }

    public List<Reserva> findAllHospedados() {
        return reservaRepository.findAllByDataSaidaIsNull();
    }

    public List<Reserva> findAllJaFizeramCheckout() {
        return reservaRepository.findAllByDataSaidaIsNotNull();
    }
}
