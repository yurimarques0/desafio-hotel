package com.senior.desafiohotel.models;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.senior.desafiohotel.enums.SIM_NAO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
public class Reserva {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Hospede hospede;

    private LocalDateTime dataEntrada;

    private LocalDateTime dataSaida;

    private SIM_NAO adicionalVeiculo;

    private Double valorUltimaHospedagem;

    private Double valorTotal;
}
