package com.senior.desafiohotel.controllers;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.senior.desafiohotel.config.RestControllerPath;
import com.senior.desafiohotel.dtos.out.HospedeDTOout;
import com.senior.desafiohotel.filters.HospedeFilter;
import com.senior.desafiohotel.models.Hospede;
import com.senior.desafiohotel.services.HospedeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(RestControllerPath.HOSPEDE_PATH)
@Api(tags = "Hospede", description = "Hospedes")
public class HospedeController {
    @Autowired
    private HospedeService hospedeService;

    @GetMapping
    @ApiOperation(value = "Extrair todos hóspedes filtrando")
    public List<HospedeDTOout> findAllFiltered(HospedeFilter filter) {
        return hospedeService.findAllFiltered(filter);
    }

    @GetMapping("/paged")
    @ApiOperation(value = "Extrair todos hóspede filtrando paginado")
    public Page<HospedeDTOout> findAllPagedFiltered(@PageableDefault(page = 0, size = Integer.MAX_VALUE) Pageable pageable,
            HospedeFilter filter) {
        return hospedeService.findAllPagedFiltered(pageable, filter);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Encontrar hóspede pelo id")
    public Optional<Hospede> findById(@PathVariable Long id) {
        return hospedeService.findById(id);
    }

    @PostMapping
    @ApiOperation(value = "Cadastrar/Atualizar um hóspede")
    public ResponseEntity<Hospede> create(@RequestBody Hospede entity, HttpServletResponse response) {
        Hospede salvo = hospedeService.create(entity);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(salvo.getId())
                .toUri();
        response.setHeader("Location", uri.toASCIIString());
        return ResponseEntity.created(uri).body(salvo);
    }


    @DeleteMapping("/{id}")
    @ApiOperation(value = "Apagar cadastro de hóspede")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable Long id) {
        hospedeService.deleteById(id);
    }

}
