package com.senior.desafiohotel.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.senior.desafiohotel.config.RestControllerPath;
import com.senior.desafiohotel.dtos.in.ReservaDTOin;
import com.senior.desafiohotel.dtos.out.ReservaDTOout;
import com.senior.desafiohotel.filters.ReservaFilter;
import com.senior.desafiohotel.models.Reserva;
import com.senior.desafiohotel.services.ReservaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(RestControllerPath.RESERVA_PATH)
@Api(tags = "Reserva", description = "Reservas")
public class ReservaController {

    @Autowired
    private ReservaService reservaService;

    @PostMapping("/check-in")
    @ApiOperation(value = "Realizar o check-in")
    public void checkIn(@RequestBody ReservaDTOin reservaDTOin) {
        reservaService.fazerCheckIn(reservaDTOin);
    }

    @GetMapping
    @ApiOperation(value = "Extrair todas reservas filtrando")
    public List<ReservaDTOout> findAllFiltered(ReservaFilter filter) {
        return reservaService.findAllFiltered(filter);
    }

    @PostMapping("/check-out")
    @ApiOperation(value = "Realizar o check-out")
    public Reserva checkOut(@RequestBody Long hospedeId) {
        return reservaService.fazerCheckOut(hospedeId);
    }

    @GetMapping("/hospedados")
    @ApiOperation(value = "Extrair todos que ainda estão hospedados no hotel")
    public List<Reserva> findAllHospedados() {
        return reservaService.findAllHospedados();
    }

    @GetMapping("/ja-fizeram-checkout")
    @ApiOperation(value = "Extrair todos que já fizeram o checkout")
    public List<Reserva> findAllJaFizeramCheckOut() {
        return reservaService.findAllJaFizeramCheckout();
    }

}
