package com.senior.desafiohotel.exceptions.entities;

import com.senior.desafiohotel.exceptions.EntityNotFoundException;

public class ReservaNotFoundException extends EntityNotFoundException {
    public ReservaNotFoundException(String message) {
        super(message);
    }

    public ReservaNotFoundException(Long id) {
        this(String.format("Não existe a reserva com o código %d ", id));
    }

}
