package com.senior.desafiohotel.exceptions.entities;

import com.senior.desafiohotel.exceptions.EntityNotFoundException;

public class HospedeNotFoundException extends EntityNotFoundException {
    public HospedeNotFoundException(String message) {
        super(message);
    }

    public HospedeNotFoundException(Long id) {
        this(String.format("Não existe o hospede com o código %d ", id));
    }

}
