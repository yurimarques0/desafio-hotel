package com.senior.desafiohotel.exceptions.entities;

import com.senior.desafiohotel.exceptions.EntityNotFoundException;

public class ParametroNotFoundException extends EntityNotFoundException {
    public ParametroNotFoundException(String message) {
        super(message);
    }

    public ParametroNotFoundException(Long id) {
        this(String.format("Não existe o parametro com o código %d ", id));
    }

}
