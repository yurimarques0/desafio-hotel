package com.senior.desafiohotel.filters;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HospedeFilter {
    private String nome;
    private String documento;
    private String telefone;

}
