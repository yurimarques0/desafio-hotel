package com.senior.desafiohotel.filters;

import com.senior.desafiohotel.enums.SIM_NAO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReservaFilter {
    private String nomeHospede;
    private String documentoHospede;
    private String telefoneHospede;
    private SIM_NAO aindaEstaoNoHotel;
}
