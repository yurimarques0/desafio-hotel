package com.senior.desafiohotel.config;

public final class RestControllerPath {

	///////////////////////////////////////////////////////////////
	// ROOT PATH
	///////////////////////////////////////////////////////////////
	public static final String ALL = "/**";
	public static final String ROOT_PATH = "/api";
	public static final String PUBLIC_ROOT_PATH = ROOT_PATH + "/public";
	public static final String PRIVATE_ROOT_PATH = ROOT_PATH + "/private";

	///////////////////////////////////////////////////////////////
	// PRIVATE PATHS
	///////////////////////////////////////////////////////////////
	public static final String HOSPEDE_PATH = PRIVATE_ROOT_PATH + "/hospedes";
	public static final String RESERVA_PATH = PRIVATE_ROOT_PATH + "/reservas";
	
	///////////////////////////////////////////////////////////////
	// PUBLIC PATHS
	///////////////////////////////////////////////////////////////
	public static final String LOGIN_PATH = PUBLIC_ROOT_PATH + "/oauth/token";
	public static final String LOGOUT_PATH = PUBLIC_ROOT_PATH + "/logout";
}
