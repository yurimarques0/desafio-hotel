package com.senior.desafiohotel.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum SIM_NAO {
	@JsonProperty("Sim")
	S("Sim"),
	@JsonProperty("Não")
	N("Não");
	private final String name;
	
	private SIM_NAO(String simNao) {
		this.name = simNao;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
}

