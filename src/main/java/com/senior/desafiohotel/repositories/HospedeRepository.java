package com.senior.desafiohotel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.senior.desafiohotel.models.Hospede;

@Repository
public interface HospedeRepository extends JpaRepository<Hospede,Long>, HospedeRepositoryQuery{
    
}
