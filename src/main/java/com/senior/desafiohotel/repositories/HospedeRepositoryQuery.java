package com.senior.desafiohotel.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.senior.desafiohotel.dtos.out.HospedeDTOout;
import com.senior.desafiohotel.filters.HospedeFilter;

public interface HospedeRepositoryQuery {
    public List<HospedeDTOout> findAllFiltered(HospedeFilter filter);
    public Page<HospedeDTOout> findAllPagedFiltered(Pageable pageable, HospedeFilter filter);
}

