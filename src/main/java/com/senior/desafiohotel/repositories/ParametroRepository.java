package com.senior.desafiohotel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.senior.desafiohotel.models.Parametro;

@Repository
public interface ParametroRepository extends JpaRepository<Parametro, Long>{
    
}
