package com.senior.desafiohotel.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.senior.desafiohotel.models.Reserva;

@Repository
public interface ReservaRepository extends JpaRepository<Reserva, Long>, ReservaRepositoryQuery {
    public Reserva findByHospedeId(Long id);

    public Optional<Reserva> findByHospedeIdAndDataSaidaIsNull(Long id);

    public List<Reserva> findAllByDataSaidaIsNull();

    public List<Reserva> findAllByDataSaidaIsNotNull();
}
