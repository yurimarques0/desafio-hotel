package com.senior.desafiohotel.repositories;

import java.util.List;

import com.senior.desafiohotel.dtos.out.ReservaDTOout;
import com.senior.desafiohotel.filters.ReservaFilter;

public interface ReservaRepositoryQuery {
    public List<ReservaDTOout> findAllFiltered(ReservaFilter filter);
}
