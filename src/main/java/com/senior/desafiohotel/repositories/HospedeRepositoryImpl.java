package com.senior.desafiohotel.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.senior.desafiohotel.dtos.out.HospedeDTOout;
import com.senior.desafiohotel.filters.HospedeFilter;
import com.senior.desafiohotel.models.Hospede;
import com.senior.desafiohotel.models.Hospede_;

public class HospedeRepositoryImpl implements HospedeRepositoryQuery {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public List<HospedeDTOout> findAllFiltered(HospedeFilter filter) {
        CriteriaBuilder cb = manager.getCriteriaBuilder();

        CriteriaQuery<HospedeDTOout> criteriaQuery = cb.createQuery(HospedeDTOout.class);
        Root<Hospede> rootFrom = criteriaQuery.from(Hospede.class);

        addFields(cb, criteriaQuery, rootFrom);

        Predicate[] predicates = addRestrictions(filter, cb, rootFrom);
        criteriaQuery.where(predicates);
        TypedQuery<HospedeDTOout> query = manager.createQuery(criteriaQuery);
        return query.getResultList();
    }

    private void addFields(CriteriaBuilder cb, CriteriaQuery<HospedeDTOout> criteriaQuery, Root<Hospede> rootFrom) {
        criteriaQuery.select(cb.construct(HospedeDTOout.class,
                rootFrom.get(Hospede_.NOME),
                rootFrom.get(Hospede_.DOCUMENTO),
                rootFrom.get(Hospede_.TELEFONE)));
    }

    private Predicate[] addRestrictions(HospedeFilter filter, CriteriaBuilder cb, Root<Hospede> rootFrom) {
        List<Predicate> predicates = new ArrayList<>();

        Predicate condition = null;

        if (!StringUtils.isEmpty(filter.getNome())) {
            condition = cb.like(rootFrom.get(Hospede_.NOME), filter.getNome() + "%");
            predicates.add(condition);
        }

        if (!StringUtils.isEmpty(filter.getDocumento())) {
            condition = cb.equal(rootFrom.get(Hospede_.DOCUMENTO), filter.getDocumento());
            predicates.add(condition);
        }

        if (!StringUtils.isEmpty(filter.getTelefone())) {
            condition = cb.equal(rootFrom.get(Hospede_.TELEFONE), filter.getTelefone());
            predicates.add(condition);
        }
        return predicates.toArray(new Predicate[predicates.size()]);
    }

    @Override
    public Page<HospedeDTOout> findAllPagedFiltered(Pageable pageable, HospedeFilter filter) {
        CriteriaBuilder cb = manager.getCriteriaBuilder();
        CriteriaQuery<HospedeDTOout> criteriaQuery = cb.createQuery(HospedeDTOout.class);
        Root<Hospede> rootFrom = criteriaQuery.from(Hospede.class);
        addFields(cb, criteriaQuery, rootFrom);
        Predicate[] predicates = addRestrictions(filter, cb, rootFrom);
        criteriaQuery.where(predicates);
        TypedQuery<HospedeDTOout> query = manager.createQuery(criteriaQuery);

        query = addPageRestrictions(query, pageable);

        return new PageImpl<>(query.getResultList(), pageable, totalOfElements(filter));
    }

    private TypedQuery<HospedeDTOout> addPageRestrictions(TypedQuery<HospedeDTOout> query, Pageable pageable) {
        int actualPage = pageable.getPageNumber();
        int totalRecordPerPage = pageable.getPageSize();
        int firstRecordOfPage = actualPage * totalRecordPerPage;
        query.setFirstResult(firstRecordOfPage);
        query.setMaxResults(totalRecordPerPage);
        return query;
    }

    private Long totalOfElements(HospedeFilter filter) {
        CriteriaBuilder cb = manager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = cb.createQuery(Long.class);
        Root<Hospede> rootFrom = criteriaQuery.from(Hospede.class);
        Predicate[] predicates = addRestrictions(filter, cb, rootFrom);
        criteriaQuery.where(predicates);

        criteriaQuery.select(cb.count(rootFrom));
        return manager.createQuery(criteriaQuery).getSingleResult();
    }

}
