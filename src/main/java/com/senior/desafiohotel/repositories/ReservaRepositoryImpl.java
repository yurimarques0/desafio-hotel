package com.senior.desafiohotel.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import com.senior.desafiohotel.dtos.out.ReservaDTOout;
import com.senior.desafiohotel.enums.SIM_NAO;
import com.senior.desafiohotel.filters.ReservaFilter;
import com.senior.desafiohotel.models.Hospede_;
import com.senior.desafiohotel.models.Reserva;
import com.senior.desafiohotel.models.Reserva_;

public class ReservaRepositoryImpl implements ReservaRepositoryQuery {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public List<ReservaDTOout> findAllFiltered(ReservaFilter filter) {
        CriteriaBuilder cb = manager.getCriteriaBuilder();

        CriteriaQuery<ReservaDTOout> criteriaQuery = cb.createQuery(ReservaDTOout.class);
        Root<Reserva> rootFrom = criteriaQuery.from(Reserva.class);

        addFields(cb, criteriaQuery, rootFrom);

        Predicate[] predicates = addRestrictions(filter, cb, rootFrom);
        criteriaQuery.where(predicates);
        TypedQuery<ReservaDTOout> query = manager.createQuery(criteriaQuery);
        return query.getResultList();
    }

    private void addFields(CriteriaBuilder cb, CriteriaQuery<ReservaDTOout> criteriaQuery,
            Root<Reserva> rootFrom) {
        criteriaQuery.select(cb.construct(ReservaDTOout.class,
                rootFrom.get(Reserva_.HOSPEDE).get(Hospede_.NOME),
                rootFrom.get(Reserva_.HOSPEDE).get(Hospede_.DOCUMENTO),
                rootFrom.get(Reserva_.HOSPEDE).get(Hospede_.TELEFONE),
                rootFrom.get(Reserva_.DATA_ENTRADA),
                rootFrom.get(Reserva_.DATA_SAIDA),
                rootFrom.get(Reserva_.ADICIONAL_VEICULO),
                rootFrom.get(Reserva_.VALOR_ULTIMA_HOSPEDAGEM),
                rootFrom.get(Reserva_.VALOR_TOTAL)));
    }

    private Predicate[] addRestrictions(ReservaFilter filter, CriteriaBuilder cb, Root<Reserva> rootFrom) {
        List<Predicate> predicates = new ArrayList<>();
        Predicate condition = null;
        if (!StringUtils.isEmpty(filter.getNomeHospede())) {
            condition = cb.like(rootFrom.get(Reserva_.HOSPEDE).get(Hospede_.NOME), filter.getNomeHospede() + "%");
            predicates.add(condition);
        }

        if (!StringUtils.isEmpty(filter.getDocumentoHospede())) {
            condition = cb.equal(rootFrom.get(Reserva_.HOSPEDE).get(Hospede_.DOCUMENTO), filter.getDocumentoHospede());
            predicates.add(condition);
        }

        if (!StringUtils.isEmpty(filter.getTelefoneHospede())) {
            condition = cb.equal(rootFrom.get(Reserva_.HOSPEDE).get(Hospede_.TELEFONE), filter.getTelefoneHospede());
            predicates.add(condition);
        }

        if (filter.getAindaEstaoNoHotel() != null && filter.getAindaEstaoNoHotel() == SIM_NAO.S) {
            condition = cb.equal(rootFrom.get(Reserva_.DATA_SAIDA), null);
        }

        return predicates.toArray(new Predicate[predicates.size()]);
    }

}
