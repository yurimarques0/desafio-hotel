package com.senior.desafiohotel.dtos.out;

import java.time.LocalDateTime;

import com.senior.desafiohotel.enums.SIM_NAO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReservaDTOout {
    private String nomeHospede;
    private String documentoHospede;
    private String telefoneHospede;
    private LocalDateTime dataEntrada;
    private LocalDateTime dataSaida;
    private SIM_NAO adicionalVeiculo;
    private Double valorUltimaHospedagem;
    private Double valorTotal;
}
