package com.senior.desafiohotel.dtos.out;

import com.senior.desafiohotel.enums.SIM_NAO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuartoDTOout {
    private Long id;
    private SIM_NAO emUso;
}
