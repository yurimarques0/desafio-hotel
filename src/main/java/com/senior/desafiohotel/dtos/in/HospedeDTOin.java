package com.senior.desafiohotel.dtos.in;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HospedeDTOin {
    private String nome;
    private String documento;
    private String telefone; 
}
