package com.senior.desafiohotel.dtos.in;

import com.senior.desafiohotel.enums.SIM_NAO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReservaDTOin {
    private Long hospedeId;
    private SIM_NAO adicionalVeiculo;
}
