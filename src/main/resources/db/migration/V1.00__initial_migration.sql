    create table parametro (
        id  bigserial not null,
            valor_diaria_dia_de_semana float8,
            valor_diaria_fim_de_semana float8,
            valor_garagem_dia_de_semana float8,
            valor_garagem_fim_de_semana float8,
            primary key (id)
    );
    create table hospede (
        id  bigserial not null,
            documento varchar(255),
            nome varchar(255),
            telefone varchar(255),
            primary key (id)
        );
    create table reserva (
       id  bigserial not null,
        adicional_veiculo int4,
        data_entrada timestamp,
        data_saida timestamp,
        valor_total float8,
        valor_ultima_hospedagem float8,
        hospede_id int8,
        primary key (id)
    );
    insert into parametro (
    valor_diaria_dia_de_semana,
    valor_diaria_fim_de_semana,
    valor_garagem_dia_de_semana,
    valor_garagem_fim_de_semana) VALUES (
        120.0,
        150.0,
        15.0,
        20.0);

    alter table reserva
       add constraint FKj8ln3danf78mmxh5u29ex7m9u
       foreign key (hospede_id)
       references hospede